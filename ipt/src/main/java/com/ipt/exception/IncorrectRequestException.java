package com.ipt.exception;

public class IncorrectRequestException extends Exception {
	public IncorrectRequestException() {
		super("The request is incorrect.");
	}
}
