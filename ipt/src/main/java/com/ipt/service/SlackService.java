package com.ipt.service;

import java.text.ParseException;

import com.ipt.exception.FormatException;
import com.ipt.exception.IncorrectRequestException;
import com.ipt.vo.SlackResponse;

public interface SlackService {

	public void send(String msg);
	public SlackResponse check(String text) throws FormatException;
	public SlackResponse book(String text, String name) throws FormatException, IncorrectRequestException, ParseException;
}
