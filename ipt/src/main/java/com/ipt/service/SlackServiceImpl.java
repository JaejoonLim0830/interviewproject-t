package com.ipt.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.ipt.dao.MyDao;
import com.ipt.exception.FormatException;
import com.ipt.exception.IncorrectRequestException;
import com.ipt.vo.SlackResponse;
import com.ipt.vo.Slot;
import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class SlackServiceImpl implements SlackService{

	private final MyDao dao;

	@Value(value = "${slack.token}")
	String token;
	@Value(value = "${slack.channel.monitor}")
	String ch;
	
	public void send(String msg) {
		try {
			MethodsClient methods = Slack.getInstance().methods(token);
            
			ChatPostMessageRequest request = ChatPostMessageRequest
            		.builder()
                    .channel(ch)
                    .text(msg)
                    .build();

            methods.chatPostMessage(request);
           
		}catch(SlackApiException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public SlackResponse check(String text) throws FormatException {
		if(pattern(text, 8)) {
			StringBuilder builder = new StringBuilder();
			dao.selectSlots(text).stream().forEach(e -> builder.append(e.toString()));
			return new SlackResponse(builder.toString());
		}else {
			throw new FormatException();
		}
	}
	
	public SlackResponse book(String text, String name) throws FormatException, IncorrectRequestException, ParseException {
		if(pattern(text, 16)) {
			if(timeCheck(text.substring(8)) && dateCheck(text.substring(0, 12))) {
				Slot timeSlot = new Slot(text, name);
				int result = dao.insertSlot(timeSlot);
				if(result == 0) {
					Slot nextAvail = dao.selectNextAvail(timeSlot).get(0);
					return new SlackResponse("Booking failed. Already booked slot. The next available time slot is after " + nextAvail.getEnd_date());
				}else {
					return new SlackResponse("Booking completed!");
				}
			}else{
				throw new IncorrectRequestException();
			}
		}else {
			throw new FormatException();
		}
	}
	
	private boolean pattern(String text, int digit) {
		return Pattern.matches("^[0-9]{" + digit + "}$", text);
	}
	
	private boolean timeCheck(String time) {
		int st = Integer.parseInt(time.substring(0, 4));
		int ed = Integer.parseInt(time.substring(4));
		
		if(st != ed && st < ed) return true;
		else return false;
	}
	
	private boolean dateCheck(String date) throws ParseException {
		DateFormat f = new SimpleDateFormat("yyyyMMddHHmm");
		Date param = f.parse(date);
		Date now = new Date();
		if(param.after(now)) return true;
		else return false;
	}
}
